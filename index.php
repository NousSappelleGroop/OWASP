<!doctype html>
<html lang="fr">
<head>
<meta charset="utf-8">

<!-- your webpage info goes here -->

    <title>Road Safety</title>
	
	<meta name="author" content="Paul&Mariam" />
	<meta name="description" content="" />

<!-- you should always add your stylesheet (css) in the head tag so that it starts loading before the page html is being displayed -->	
	<link rel="stylesheet" href="style/style.css" type="text/css" />
	
</head>
<body>

<!-- webpage content goes here in the body -->

	<div id="page">
		<div id="logo">
			<h1><a href="/" id="logoLink">Road Safety</a></h1>
		</div>
		<div id="nav">
			<ul>
				<li><a href="index.php">Accueil</a></li>
				<li><a href="vulns/vuln1/index.php">A1 - Injection</a></li>
				<li><a href="vulns/vuln2/index.php">A2 - Broken Authentication and Session Management</a></li>
				<li><a href="vulns/vuln3/index.php">A3 - Cross-Site Scripting (XSS)</a></li>
				<li><a href="vulns/vuln4/index.php">A4 - Broken Access Control</a></li>
				<li><a href="vulns/vuln5/index.php">A5 - Security Misconfiguration</a></li>
				<li><a href="vulns/vuln6/index.php">A6 - Sensitive Data Exposure</a></li>
				<li><a href="vulns/vuln7/index.php">A7 - Insufficient Attack Protection</a></li>
				<li><a href="vulns/vuln8/index.php">A8 - Cross-Site Request Forgery (CSRF)</a></li>
				<li><a href="vulns/vuln9/index.php">A9 - Using Components with Known Vulnerabilities</a></li>
				<li><a href="vulns/vuln10/index.php">A10 - Underprotected APIs</a></li>
			</ul>
		</div>
		<div id="content">
			<h2>Acceuil</h2>
			<p>
				Ce site a pour but de tester les 10 vulnérabilités du top 10 2017 d'owasp.
			</p>
			<p> 
				Pour cela nous avons mis une vulnérabilité sur chaque page sauf la page d'accueil. Amusez vous bien !
			</p>
		</div>
		<div id="footer">
			<p>
				Webpage made by <a href="/" target="_blank">[Paul&Mariam]</a>
			</p>
		</div>
	</div>
</body>
</html>